import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MainKtTest {



    @Test
    fun checkPasswordIsValid() {
        assertTrue(checkPassword("lkjsd!kj4F"))
    }
    @Test
    fun checkPasswordIsInvalid() {
        assertFalse(checkPassword("lkkj4F"))
    }
    @Test
    fun checkPasswordHaUperCase() {
        assertFalse(checkPassword("lkjsd!kj4"))
    }
    @Test
    fun checkPasswordHasSymbol() {
        assertFalse(checkPassword("lkjsdkj4F"))
    }



    @Test
    fun checkIfUserNameIsInList(){
        assertTrue(checkUsername("David"))
    }

    @Test
    fun checkIfUserNameIsNotInList(){
        assertFalse(checkUsername("Javi"))
    }
}