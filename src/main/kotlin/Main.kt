import java.util.Scanner

/**
 * @version 1.0
 * @author Diego Arteaga
 */


var userList = mutableMapOf<String, String>("Jordi" to "qwertY123", "Jose" to "123456Jose", "David" to "DaWeD4B3sT")

/**
 * @param password parametre String d'entrada per la funcio checkPassword que s'utilitza per validar si compleix les condicions que demana la funcio
 * @return retorna un boolea si es compleix si te te minuscules y majuscules digits i simbols
 * @author Diego Arteaga text específic de l'etiqueta
 */

fun checkPassword(password: String): Boolean {
    var lowerCase = false
    var upperCase = false
    var digit = false
    var symbol = false
    if (password.length < 8) return false
    for (character in password) {
        if (character in 'a'..'z') lowerCase = true
        if (character in 'A'..'Z') upperCase = true
        if (character in '0'..'9') digit = true
        if (character in '!'..'/' || character in ':'..'@' || character in '['..'`' || character in '{'..'~') symbol = true
    }
    return lowerCase && upperCase && digit && symbol
}

/**
 * @param username parametre String d'entrada amb la que es comproba si userList no conte el parametre
 * @return retorna un boolea si la lista no conte el parametre
 * @author Diego Arteaga text específic de l'etiqueta
 */
fun checkUsername(username: String): Boolean {
    return userList.contains(username)
}

/**
 * @param username paramentre String que utilitza pero reemplazar un usuari a la userList si les funcions checkUsername i check password son false
 * @param password paramentre String que utilitza pero reemplazar un usuari a la userList si les funcions checkUsername i check password son false
 * @return retorna un boolea si es compleix si te te minuscules y majuscules digits i simbols
 * @author Diego Arteaga text específic de l'etiqueta
 */
fun addUser(username: String, password: String) {
    userList.put(username, password)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Welcome to ITB!!")
    println("Do you want to register a user? (yes/no)")
    while (scanner.next().lowercase() == "yes") {
        println("Insert a username:")
        val username = scanner.next()
        println("Insert a password:")
        val password = scanner.next()
        if (!checkUsername(username) && checkPassword(password)) {
            addUser(username, password)
            println("User added to system")
        } else println("Username is repeated or password is not correct")
        println("Do you want to register another user? (yes/no)")
    }
}